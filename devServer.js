/**
 * Created by kingHenry on 12/6/15.
 */
var path = require('path');
var express = require('express');
var webpack = require('webpack');
var config = require('./webpack.config.dev');
var routes = require('./routes/index');
var passport = require('passport');

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var r = require('rethinkdb');
var rethinkConfig = require('./app/config.js');
var app = express();
var compiler = webpack(config);
var session = require('express-session');
var flash    = require('connect-flash');
var methodOverride = require('method-override');
var User   = require('./app/models/users'); // get our thinky model
require('./config/passport')(passport);

//var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens


function handleError(res, error) {
    return res.send(500, {error: error.message});
}
function createConnection(req, res, next) {
    r.connect(rethinkConfig.rethinkdb, function(error, conn) {
        if (error) {
            handleError(res, error);
        }
        else {
            // Save the connection in `req`
            req._rdbConn = conn;
            // Pass the current request to the next middleware
            next();
        }
    });
}

app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({ extended: true }));    // parse application/x-www-form-urlencoded
app.use(bodyParser.json());    // parse application/json


// routes ======================================================================


app.use(session({ secret: 'someshittysecret' ,cookie: { maxAge: 60000 },resave: true,
    saveUninitialized: true}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash());
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport
app.use(createConnection);
app.use('/', routes);

app.use(function(req, res, next){
    res.locals.success_messages = req.flash('success_messages');
    res.locals.error_messages = req.flash('error_messages');
    next();
});
//require('./routes/index.js')(app); // load our routes and pass in our app and fully configured passport
app.get('/setup', function(req, res) {

    // create a sample user
    var sriram = new User({
        email: 'sriram@flowopt.com',
        password: 'password',
        admin: true
    });

    // save the sample user
    sriram.save(function(err) {
        if (err) throw err;

        console.log('User saved successfully');
        res.json({ success: true });
    });
});
app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));
app.use(express.static('public'));




// route middleware to make sure a user is logged in
//function isLoggedIn(req, res, next) {
//
//    // if user is authenticated in the session, carry on
//    if (req.isAuthenticated())
//        return next();
//
//    // if they aren't redirect them to the home page
//    res.redirect('/');
//}

app.listen(3000, '0.0.0.0', function(err) {
    if (err) {
        console.log(err);
        return;
    }

    console.log('Listening at http://flowopt.such.cool:3000');
});
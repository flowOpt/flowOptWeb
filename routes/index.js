var express = require('express');
var router = express.Router();
var r = require('rethinkdb');
var rethinkConfig = require('../app/config.js');
var Handlebars = require('handlebars');
var waitTimes = require('../app/rethink.js');
var session = require('express-session');
var flash    = require('connect-flash');
var User   = require('../app/models/users'); // get our thinky model
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var jwt=require('jsonwebtoken');


/* GET home page. */

router.get('/acuityWait/getAll/',function(req, res, next) {
    r.table('ebb1')
        .between(1,8, {index:"dayOfWeek"})
        .withFields('acuityInitial','dayOfWeek','hour','waitTime','year','month','disposition')
        .run(req._rdbConn).then(function(re){
            return re.toArray()
        }).then(function(data){
            res.send(JSON.stringify(data));
        });
});

router.get('/users', function(req, res) {
    User.run().then( function(users) {
        res.json(users);
    });
});

router.get('/acuityWait/get/:type',function(req, res, next) {
    var acuityType=Number(req.params.type);
    r.table('ebb1')
        .get_all(acuityType, {index:"acuityInitial"})
        .pluck("acuityInitial","waitTime","hour","dayOfWeek","dayNum","year")
        .run(req._rdbConn, function(error, cursor) {
            if (error) {
                handleError(res, error);
                next();
            }
            else {
                // Retrieve all the results
                cursor.toArray(function(error, result) {
                    if (error) {
                        handleError(res, error);
                    }
                    else {
                        // Send back the data as json
                        res.send(JSON.stringify(result));
                    }
                });
            }
        });
});



router.get('/fixed',function(req,res,next){
   res.render('fixedHeader.jade',{title:'flowOpt'});
});


router.get('/dcTest',function(req,res,next){
    res.render('dcTest.jade',{title:'DC Test'})
});

router.param('acuityFloat',function(req,res,next,acuityFloat){
   next();
});



module.exports = router;

*There is  Vagrantfile for building a virtual machine to run this application**
=================================================================================


REQUIREMENTS:
-------------
* at least 6GB of ram (preferably >=8)
* at least a quad-core machine (2 cores allocated to the VM that will be spawned)
Download virtualBox in order for vagrant to work:

[Virtual Box Downloads page](https://www.virtualbox.org/wiki/Downloads)

Then you can install vagrant:

[Vagrant download page](https://www.vagrantup.com/downloads.html)

Install both for your respective platform (windows, osx, etc)

To set the hosts accordingly in order to get oAuth working install the hosts plugin for vagrant:

[Vagrant-Hostsupdater](https://github.com/cogitatio/vagrant-hostsupdater)

To Install:

 `vagrant plugin install vagrant-hostsupdater`
 
 --------------------
 GIT CLONE this REPO!
 --------------------
 You may have to [ADD AN SSH KEY](http://doc.gitlab.com/ce/ssh/README.html)
 
 If you don't add your public ssh key you will ahve to clone using the HTTP link 
 instead of the ssh link and enter your username/password anytime you do any git operations.
 
 In a terminal window, navigate to the location of the repository you have just cloned onto your HOST machine.
----  -----
*Note* 
------
 HOST is your LAPTOP. GUEST is the virtual machine running the development environment.
-----                 -----
Then you will be able to run `vagrant up --provider=virtualbox`

If you are using VMWare Fusion you can run `vagrant up --provider=vmware_fusion`




Installation Steps:
-------------

After the development env is created, you can log into the system by running the command 

`vagrant ssh`

this command must be run in the folder /flowOptWeb/ as it uses the Vagrantfile to determine the ssh address of the guest machine.

*Note*
------

`/flowOptWeb/` is a mounted folder. This means that if you edit any files on your local box, virtual box
will reload them automatically. 
You do not have to `scp` them into the VM in order to upload updated files.

Your folder `flowOptWeb/` on local will always have the same contents as `/vagrant/` on the virtual machine
that has been created.

Once you are inside the GUEST machine, you should see a linux prompt.

Here you can type

`cd /vagrant/`

This is your project root. This is the location on the GUEST machine that mirrors `/flowOptWeb/` on the HOST.

**From this point on, all commands will be entered in the GUEST machine, in the /vagrant/ home DIR**

Sometimes there are issues with source-ing the .bashrc file, so just in case it never hurts to run

`source ~/.bashrc` 

one more time just for good measure to ensure all symlinks/env variables are in place and active.

Make sure to install all required NPM libraries, by running 

`npm install`

For some reason, babel-core doesn't install via npm-shrinkwrap.json, so manually enter:

`npm install --save babel-core@5.8`

To run the app run `npm start`

It should be visible in your browser at [dev.flowOpt.com:3000](http://dev.flowOpt.com:3000)
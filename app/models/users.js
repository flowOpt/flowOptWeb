var thinky = require('thinky')({
    host: '159.203.136.159',
    port: 28015,
    authKey: '',
    db: 'flowopt'
});
var type = thinky.type;
var bcrypt   = require('bcrypt-nodejs');
var R = require('ramda');

exports.Users = thinky.createModel( "User",{
    emailUser:type.string(),
    password:type.string(),
    admin:type.boolean().default(false)
});
var Google = thinky.createModel(
"Google",{
        id:type.string(),
        email:type.string(),
        token: type.string(),
        name: type.string(),
        admin:type.boolean()
    });

Google.belongsTo(exports.Users, "User","email","emailUser");



exports.Users.comparePassword = function(password, user, callback) {

    bcrypt.compare(password, user.password, function(err, match) {

        if (err) callback(err);

        if (match) {
            callback(null, true);
        } else {
            callback(err);
        }
    });
};

exports.Users.isAdmin = function(user){

};




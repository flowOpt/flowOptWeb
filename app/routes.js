// app/routes.js
var User            = require('../app/models/users').Users;
module.exports = function(app, passport) {

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================

    app.get('/', function(req, res, next) {
        res.render('index.ejs', { title: 'flowOpt - Home', user:req.user});
    });
    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    app.get('/login', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('login.ejs',{title:'flowOpt - Login',message: req.flash('loginMessage') });
    });

    // process the login form
    // app.post('/login', do all our passport stuff here);

    // =====================================
    // SIGNUP TEMPORARILY DISABLED==========
    // =====================================
    // show the signup form
    //app.get('/signup', function(req, res) {
    //
    //    // render the page and pass in any flash data if it exists
    //    res.render('signup.ejs', { message: req.flash('signupMessage') });
    //});

    // process the signup form
    // app.post('/signup', do all our passport stuff here);
    //app.post('/signup', passport.authenticate('local-signup', {
    //    successRedirect : '/analytics', // redirect to the secure profile section
    //    failureRedirect : '/signup', // redirect back to the signup page if there is an error
    //    failureFlash : true // allow flash messages
    //}));

    // =====================================
    // PROTECTED ROUTES ====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/analytics',isLoggedInAsAdmin,function(req,res,next){
        res.render('analytics.ejs',
            {title:'Analytics',
            user:req.user// get the user out of session and pass to template
        })
    });


    // =====================================
    // FACEBOOK ROUTES =====================
    // =====================================
    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {scope:['email','public_profile'],
            successRedirect : '/analytics',
            failureRedirect : '/'
        }));

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    // =====================================
    // LOGIN ===============================
    // =====================================
    // process the login form
    app.post('/authenticate', passport.authenticate('local-login', {
        successRedirect : '/analytics', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

};
//User.get(id)
//    .run()
//    .then(function(user) {
//        done(null, user);
//    });

// route middleware to make sure a user is logged in
function isLoggedInAsAdmin(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        if(req.user.admin){
            return next();
        }res.redirect('/');
    }res.redirect('/login',{user:req.user});

    // if they aren't redirect them to the home page

}

var React = require('react'),
    ReactDOM = require('react-dom'),
    d3 = require('d3'),
    _ = require('lodash');

var MetaMixin = {
    getYears: function(data){
        data || (data = this.props.data);

        return _.keys(
            _.groupBy(this.props.data,
            function(d){
                return d.state;
            })
        )
    },
    getFormatter: function(data){
        data || (data = this.props.data);
        return d3.scale.linear()
                .domain(d3.extent
                (this.props.data,
                function(d){
                    return d.base_salary;
                })).tickFormat();
    },
    getAllDataByYear: function(year,data){
        data || (data = this.props.data);

        return data.filter(function(d){
            return d.state ==year;
        });
    }
};

var Title = React.createClass( {
    mixins:[MetaMixin],
    getYearsFragment : function() {
        var years = this.getYears(),
        fragment;

        if(years.length > 1){
            fragment = "";
        }else {
            fragment = " in " + years[0];
        }

        return fragment;
    },
    render: function(){
        var mean = d3.mean(this.props.data,
        function(d){
            return d.hour;
        }),
            format =this.getFormatter();

        var yearsFragment = this.getYearsFragment();
        return(
            <h2> Patients  have an average wait time of &nbsp;
            {format(mean)} minutes</h2>)

    }
});

var Description = React.createClass({
    mixins:[MetaMixin],
    getYearFragment : function(){
        var years = this.getYears(),
            fragment;

        if(years.length > 1){
            fragment = "";
        }else {
            fragment = "";
        }

        return fragment;
    },
    render:function(){
        var formatter = this.getFormatter(),
            mean = d3.mean(this.props.data,
            function(d){
              return d.hour;
            }),
            deviation = d3.deviation(this.props.data,
            function(d){
                return d.hour;
            });
        var yearFragment = this.getYearFragment();
        return (
            <p className ="lead">Average wait times vary among data collected from {formatter(this.props.data.length)} patients.
                    wait times vary between {formatter(mean-deviation)} and {formatter(mean+deviation)}
            </p>
        )
    }
});

module.exports = {
    Title: Title,
    Description: Description
};
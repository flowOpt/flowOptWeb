/**
 * Created by kingHenry on 1/4/16.
 */
// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;

// load up the user model
var User            = require('../app/models/users').Users;

var bcrypt=require('bcrypt-nodejs');


// expose this function to our app using module.exports
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        User.get(id)
            .run()
            .then(function(user) {
                done(null, user);
            });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'
    passport.use('local-signup', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) {
            var emailField = email;
            var password = bcrypt.hashSync(password);
            process.nextTick(function() {
                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                User.getAll(emailField, {index: 'emailUser'}).run().then(function(err, userArray) {
                    // if there are any errors, return the error
                    //var user = userArray[0];
                    // check to see if theres already a user with that email
                    if (userArray) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {
                        console.log('saving user...');
                        // if there is no user with that email
                        // create the user
                        var newUser = new User({
                            emailUser:emailField,
                            password:password,
                        });
                        newUser.save().then(function(userInfo){
                            return done(null,newUser);
                        })
                    }

                });

            });
        })),
    // =========================================================================
    // LOCAL Log-In ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'
    passport.use('local-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, email, password, done) {
            var emailField = email;
            var password = password;
            // asynchronous
            // User.findOne wont fire unless data is sent back
            process.nextTick(function () {
                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                User.getAll(emailField, {index: 'emailUser'}).run().then(function (userArray) {
                    // check to see if theres already a user with that email
                        var user = userArray[0];
                        if (!user) {
                            return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
                            console.log('no user found');
                        }
                        User.comparePassword(password, user, function (err, valid) {
                            if (err) {
                                return done(null, false, req.flash('loginMessage', err));
                                console.log(err);
                            }
                            if (!valid) {
                                return done(null, false, req.flash('loginMessage', 'The username or password is incorrect!'));
                                console.log('username or password incorrect!');
                            } else {
                                console.log("logged in!!");
                                return done(null,user)
                            }
                        })


                })
            })
        }))
};








/**
 * Created by kingHenry on 1/11/16.
 */
module.exports = {

    'facebookAuth' : {
        'clientID'      : '1645347689044722', // your App ID
        'clientSecret'  : 'e3a20e53f52c4dbf16a13b8a7e93855c', // your App Secret
        'callbackURL'   : 'http://localhost:3000/auth/facebook/callback'
    },

    'googleAuth' : {
        'clientID'      : 'your-secret-clientID-here',
        'clientSecret'  : 'your-client-secret-here',
        'callbackURL'   : 'http://localhost:8080/auth/google/callback'
    }

};
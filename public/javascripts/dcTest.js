//var dataTable = dc.dataTable("#dc-table-graph");
var acuityChart = dc.pieChart("#dc-acuity-chart");
var depthChart = dc.barChart("#dc-depth-chart");
var dayOfWeekChart = dc.rowChart("#dc-dayweek-chart");
var acuityPieChart = dc.pieChart("#dc-acuityPie-chart");
var timeChart = dc.lineChart("#dc-time-chart");
var moveChart = dc.barChart('#monthly-move-chart');
//var dataToExport = dc.top(Infinity);
//var exportData = console.log(timeChart.top(Infinity));


d3.csv("/data/acuityData.csv", function (data) {

  // format our data
  var dtgFormat = d3.time.format("%m-%Y");
  var dtgFormat2 = d3.time.format("%a %e %b %H");
  
  data.forEach(function(d) {
    d.dtg1=d.dayOfWeek;
    d.dtg= dtgFormat.parse(d.month+"-"+d.year)  ;
    d.dtg2=d.year;
    d.mag= d.acuityInitial;
    d.depth= d.waitTime;
    d.dispositions= d.disposition;

  });


  // Run the data through crossfilter and load our 'facts'
  var facts = crossfilter(data);
  var all = facts.groupAll();

// Dimension by month
  var moveMonths = facts.dimension(function (d) {
    return d.dtg;
  });
  // Group by total movement within month
  var monthlyMoveGroup = moveMonths.group().reduceCount(function (d) {
    return (d.dtg);
  });

  // for acuityInitial
  var magValue = facts.dimension(function (d) {
    return d.mag;       // add the magnitude dimension
  });
  var magValueGroupSum = magValue.group()
    .reduceSum(function(d) { return d.mag; });	// sums
  var magValueGroupCount = magValue.group()
    .reduceCount(function(d) { return d.mag; }) // counts

  // for wait time
  var depthValue = facts.dimension(function (d) {
    return d.depth;
  });
  var depthValueGroup = depthValue.group();

  // time chart
  var volumeByHour = facts.dimension(function(d) {
    return(d.dtg);
  });
  var volumeByHourGroup = volumeByHour.group()
    .reduceCount(function(d) {
      return(d.dtg);
    });

   //row chart Day of Week
  var dayOfWeek = facts.dimension(function (d) {
    var day=d.dtg1;
    return day
  });
  var dayOfWeekGroup = dayOfWeek.group();

  var dispositions = facts.dimension(function (d){
    if (d.dispositions ==0)
      return "Discharged";
    else if (d.dispositions ==1)
    return "Admitted";
    else return "undefined"
  });

  var dispositionsGroup=dispositions.group();
  // Pie Chart
  var acuities = facts.dimension(function (d) {
    if (d.acuityInitial ==1.0)
      return "1.0";
    else if (d.acuityInitial==2.0)
      return "2.0";
    else if (d.acuityInitial==3.0)
      return "3.0";
    else if (d.acuityInitial==4.0)
      return "4.0";
    else if (d.acuityInitial==5.0)
      return "5.0";
    });
  var acuitiesGroup = acuities.group();


  // Create datatable dimension
  //var timeDimension = facts.dimension(function (d) {
  //  return d.dtg;
  //});
  // Setup the charts

  // count all the facts
  dc.dataCount(".dc-data-count")
    .dimension(facts)
    .group(all);

  // Disposition pie chart
  acuityChart.width(250)
    .height(220)
    .radius(100)
    .innerRadius(30)
      .dimension(dispositions)
      //.brushOn(false)
      .title(function(d){return d.value;})
	.group(dispositionsGroup);

  // islands pie chart
  acuityPieChart.width(250)
      .height(220)
      .radius(100)
      .innerRadius(30)
      .dimension(acuities)
      //.brushOn(false)
      .title(function(d){return d.value;})
      .group(acuitiesGroup);

  // Depth bar graph
  depthChart.width(930)
    .height(150)
   // .mouseZoomable(true)
    .margins({top: 10, right: 10, bottom: 20, left: 40})
    .dimension(depthValue)
    .group(depthValueGroup)
	.transitionDuration(500)
    .centerBar(true)	
	.gap(1)  
    .x(d3.scale.linear().domain([0, 500]))
	.elasticY(false)
	.xAxis().tickFormat(function(v) {
    $( "#exportData" ).unbind().click(function() {
      download(d3.csv.format(depthValue.top(Infinity)),
          "exportedData.csv");
    });
    return v;});

  moveChart
      .width(900)
      .height(40)
      .margins({top: 0, right: 50, bottom: 20, left: 40})
      .dimension(moveMonths)
      .group(monthlyMoveGroup)
      .centerBar(true)
      .gap(1)
      .mouseZoomable(true)
      .x(d3.time.scale().domain(d3.extent(data, function(d) { return d.dtg; })))
      .xUnits(d3.time.months);

  moveChart.yAxis().ticks(0);

  // time graph
  timeChart.width(900)
    .height(130)
    .transitionDuration(500)
    //.mouseZoomable(true)
    .margins({top: 10, right: 10, bottom: 20, left: 40})
    .dimension(volumeByHour)
    .group(volumeByHourGroup)
    .brushOn(false)			// added for title
    .title(function(d){
      return (d.key)
      + "\nNumber of Events: " + d.value;
      })
    .elasticX(true)
    .x(d3.time.scale().domain(d3.extent(data, function(d) { return d.dtg; })))
      //.x(d3.time.scale().domain([new Date(2010, 0, 1), new Date(2014, 11, 31)]))
    .xAxis();

  var dayDict={1:"Sun",2:"Mon",3:"Tues",4:"Wed",5:"Thurs",6:"Fri",7:"Sat"};
  // row chart day of week
  dayOfWeekChart.width(250)
    .height(220)
    .margins({top: 5, left: 10, right: 10, bottom: 20})
    .dimension(dayOfWeek)
    .group(dayOfWeekGroup)
    .colors(d3.scale.category10())
    .label(function (d){
      return dayDict[d.key];
    })
    //.brushOn(false)
    .title(function(d){return d.value;})
    .elasticX(false)
    .xAxis().ticks(4);



  // Table of acuity data
  //dataTable.width(960).height(800)
  //  .dimension(timeDimension)
	//.group(function(d){return " "})
  //  .size(50)
  //  .columns([
  //    function(d) {
  //      return d.acuityInitial;},
  //    function(d) { return d.depth;},
  //    function(d) { return d.dtg; },
  //    function(d) {return d.dtg1;},
  //    function(d) { return d.dtg2;},
  //
  //  ])
  //  .sortBy(function(d){ return d.waitTime; })
  //  .order(d3.descending);



  //function exportData(allData) {
  //   dataTable.width(960).height(800)
  //      .dimension(timeDimension)
  //      .group(function(d){return " "})
  //      .size(Infinity)
  //      .columns([
  //        function(d) { return d.acuityInitial; },
  //        function(d) { return d.depth;},
  //        function(d) { return d.dtg; },
  //        function(d) {return d.dtg1;},
  //        function(d) { return d.dtg2;}
  //      ])
  //      .sortBy(function(d){ return d.waitTime; })
  //      .order(d3.descending);
  //  d3.csv.format(allData);
  //};
  // Render the Charts
  dc.renderAll();
  //$("#monthly-move-chart>svg>g>g.y").attr("visibility","hidden");

});